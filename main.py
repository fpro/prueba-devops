import requests
import random
from bs4 import BeautifulSoup
import urllib.request



def run():
    for c in range(1, 5):
        number_random = random.randrange(1,50,1) 
        print(number_random)
        response = requests.get('https://xkcd.com/{}'.format(number_random))
        soup = BeautifulSoup(response.content, 'html.parser')
        image_container = soup.find(id='comic')

        image_url = image_container.find('img')['src']
        image_name = image_url.split('/')[-1]
        print('Descargando la imagen {}'.format(image_name))
        urllib.request.urlretrieve('https:{}'.format(image_url), image_name)


if __name__ == '__main__':
    run() 